//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

struct Contato {
    let nome: String
    let email: String
    let endereco: String
    let telefone: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        celula.nome.text = contato.nome
        celula.email.text = contato.email
        celula.endereco.text = contato.endereco
        celula.telefone.text = contato.telefone
        
        return celula
    }
    

    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        tableview.delegate = self
        
        listaDeContatos.append(Contato(nome: "Contato 1", email: "contato1@gmail.com", endereco: "Rua do Endereco, 1",telefone: "00000-0000"))
        listaDeContatos.append(Contato(nome: "Contato 2", email: "contato2@gmail.com", endereco: "Avenida do Endereco, 2",telefone: "11111-1111"))
        listaDeContatos.append(Contato(nome: "Contato 3", email: "contato3@gmail.com", endereco: "Alameda do Endereco, 3",telefone: "22222-2222"))
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "abrirDetalhes" {
        let detalhesViewController = segue.destination as! DetalhesViewController
        let contato = sender as! Contato
        detalhesViewController.contato = contato
        }else if segue.identifier == "criarContato"{
            let novoContatoViewController = segue.destination as! NovoContatoViewController
            novoContatoViewController.delegate = self
        }
  }
}

extension ViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contato = listaDeContatos[indexPath.row]
        performSegue(withIdentifier: "abrirDetalhes", sender: contato)
    }
}

extension ViewController: NovoContatoViewControllerDelegate{
    func salvarNovoContato(contato: Contato) {
        listaDeContatos.append(contato)
        tableview.reloadData()
    }
}
